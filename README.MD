## Weborama: assignement JavaScript (React) ##

### Tags ###
_JavaScript,_ _React,_ _React-Router,_ _Webpack_

### Description ###
This app fetches a list of PUNK API beers from a public API and displays a list in a table in the browser.
We use this assignment to get a view of the JavaScript and problem solving skills of the applicant. The assignment should not take longer than two hours to complete.

### Deliver ###
Please deliver a GitLab or GitHub link to your work. Create for every feature a new branch. When this feature is completed, merge it to the master branch and increment the minor version number. Preferably create a git tag with this version number.

### Assignment ###
1. Clone this git repository.
2. Install all node modules and than run the app by running `npm start` in your console
3. In the app you see a table with a list of Punk IPA beers. Above there is a filter, but it is not working; make the filter working so the user can filter on the name of the beer.
4. Create a new component: "Button", based on bootstrap button. (see other components);
5. Update the table component and make the "striped" option a property, so we can choose if we want the table striped or not.
6. Add the Button component to the app, Make it toggle the table to striped and not striped.
7. *OPTIONAL:* Make the name of the beer clickable. Create a new route /beer/:id to react-router and display information about the selected beer on that route.

### Usefull links ###
[Punk ipa api documentation](https://punkapi.com/documentation/v2)  
[Bootstrap documentation](https://getbootstrap.com/docs/4.0/components/)   
[Google](https://www.google.nl)


### Contact ###
If you have any questions, please contact:

Jasper Prins or Xander schouwerwou   
tier2support [@] weborama . nl