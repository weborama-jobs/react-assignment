import {dispatch} from 'dispatchers/AppDispatcher';
import Constants from 'constants/PunkIPA';

const PunkIPA = {
  /**
   * Dispatches an action to fetch a list of Punk IPA's
   */
  list() {
    dispatch({
      actionType: Constants.LIST,
    });
  },
};

export default PunkIPA;
