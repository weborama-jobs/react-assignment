import {EventEmitter} from 'events';

const CHANGE_EVENT = 'change';
const ERROR_EVENT = 'error';
/**
 * Base classe for stores
 */
const Base = Object.assign({}, EventEmitter.prototype, {
  /**
   * Add event listener on CHANGE event.
   * @param  {Function} callback Event Listener callback
   */
  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  /**
   * Removes event Listener
   * @param  {Function} callback callback
   */
  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
  /**
   * Adds error event handler
   * @param  {Function} callback callback
   */
  addErrorListener(callback) {
    this.on(ERROR_EVENT, callback);
  },
  /**
   * Removes event listener
   * @param  {Function} callback callback
   */
  removeErrorListener(callback) {
    this.removeListener(ERROR_EVENT, callback);
  },
  /**
   * emits event
   * @param  {Object}   data change message
   */
  emitChange(data) {
    this.emit(CHANGE_EVENT, data);
  },
  /**
   * Emits an error event
   * @param  {Object}   data error message
   */
  emitError(data) {
    this.emit(ERROR_EVENT, data);
  },
  /**
   * Makes an ajax request
   * @param {Object} userSettings User settings
   */
  call(userSettings) {
    const defaultSettings = {
      dataType: 'json',
      type: 'POST',
      cache: true,
      processData: true,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      onError() {},
    };

    const settings = {
      success: userSettings.onSuccess,
      error: userSettings.onError,
    };

    Object.assign(settings, defaultSettings, userSettings);

    return $.ajax(settings);
  },
});

// set max listeners to unlimited to avoid node warning:
// (node) warning possible eventemitter memory leak detected. 11 listeners added
Base.setMaxListeners(0);

export default Base;
