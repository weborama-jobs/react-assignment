import Base from 'stores/Base';
import PunkIPA from 'constants/PunkIPA';
import {register} from 'dispatchers/AppDispatcher';
/**
 * PunkIPAStore
 */
const PunkIPAStore = Object.assign({}, Base, {
  beers: [],
  /**
   * Returns a list of punk ipa's
   */
  getBeers() {
    return this.beers;
  },
  /**
   * Get's a list of Punk IPA's
   */
  list() {
    const onSuccess = (response) => {
      this.beers = response.slice();
      this.emitChange({action: PunkIPA.LIST});
    };
    const onError = (message) => {
      this.emitError({action: PunkIPA.LIST, message});
    };
    this.call({
      method: 'GET',
      url: 'https://api.punkapi.com/v2/beers',
      onSuccess,
      onError,
    });
  },
  dispatcherIndex: register((action) => {
    switch (action.actionType) {
      case PunkIPA.LIST:
        PunkIPAStore.list();
        break;
      //  no default
    }
    return true; // No errors. Needed by promise in Dispatcher.
  }),
});

export default PunkIPAStore;
