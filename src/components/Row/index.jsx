import React from 'react';
import PropTypes from 'prop-types';
/**
 * Bootstrap Grid row
 * @param {Object} props props
 */
const Row = props => (
  <div className="row">
    {props.children}
  </div>
);

export default Row;

Row.propTypes = {
  children: PropTypes.node,
};
