import React from 'react';
import PropTypes from 'prop-types';
/**
 * Input component
 * @param {Object} props properties
 */
const Input = props => (
  <input
    type="text"
    className="form-control"
    placeholder={props.placeHolder}
    onChange={(e) => { props.onChange && props.onChange(e.target.value); }}
  />
);

export default Input;

Input.propTypes = {
  placeHolder: PropTypes.string,
  onChange: PropTypes.func,
};
