import React from 'react';
import PunkIPAStore from 'stores/PunkIPAStore';
import PunkIPA from 'actions/PunkIPA';
import Container from 'components/Container';
import Row from 'components/Row';
import Column from 'components/Column';
import Input from 'components/Input';
import BeerListTable from 'components/BeerListTable';
/**
 * Class BeerList
 */
class BeerList extends React.Component {
  /**
   * Constructor
   */
  constructor() {
    super();

    this.state = {
      error: false,
      beers: [],
    };

    this.onBeerStoreChange = this.onBeerStoreChange.bind(this);
    this.onBeerStoreError = this.onBeerStoreError.bind(this);
  }
  /**
   * Register store event listeners
   */
  componentWillMount() {
    PunkIPAStore.addChangeListener(this.onBeerStoreChange);
    PunkIPAStore.addErrorListener(this.onBeerStoreError);
  }
  /**
   * Fetch a list of Punk IPA's
   */
  componentDidMount() {
    PunkIPA.list();
  }
  /**
   * Unregister store event listeners
   */
  componentWillUnmount() {
    PunkIPAStore.removeChangeListener(this.onBeerStoreChange);
    PunkIPAStore.removeErrorListener(this.onBeerStoreError);
  }
  /**
   * BeerStore change listeners
   */
  onBeerStoreChange() {
    this.setState({beers: PunkIPAStore.getBeers()});
  }
  /**
   * BeerStore error listener
   */
  onBeerStoreError() {
    this.setState({error: true});
  }
  /**
   * Renders a table with a list of beers
   */
  render() {
    return (
      <Container>
        <Row>
          <Column>
            <img alt="Punk ipa" src="images/punk.png" />
          </Column>
        </Row>
        <Row>
          <Column>
            {this.state.error &&
              <div> An error occured fetching a list of Punk IPA</div>
            }
            {!this.state.error &&
              <div>
                <Input
                  placeHolder="Filter IPA's"
                  onChange={() => { }}
                />
                <BeerListTable beers={this.state.beers} />
              </div>
            }
          </Column>
        </Row>
      </Container>
    );
  }
}

export default BeerList;
