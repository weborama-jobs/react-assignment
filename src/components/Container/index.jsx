import React from 'react';
import PropTypes from 'prop-types';
/**
 * Bootstrap Grid container
 * @param {Object} props props
 */
const Container = props => (
  <div className="container">
    {props.children}
  </div>
);

export default Container;

Container.propTypes = {
  children: PropTypes.node,
};
