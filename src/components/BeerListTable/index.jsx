import React from 'react';
import PropTypes from 'prop-types';
import Table from 'components/Table';
/**
 * Table with a list of beers
 * @param {Object} props properties
 */
const BeerListTable = props => (
  <Table>
    <thead>
      <tr>
        <th>name</th>
        <th>description</th>
      </tr>
    </thead>
    <tbody>
      {
        props.beers.map(beer => (
          <tr key={beer.id}>
            <td>{beer.name}</td>
            <td>{beer.description}</td>
          </tr>
        ))
      }
    </tbody>
  </Table>
);

export default BeerListTable;

BeerListTable.propTypes = {
  beers: PropTypes.array,
};
