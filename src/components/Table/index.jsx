import React from 'react';
import PropTypes from 'prop-types';
/**
 * Bootstrap table component
 * @param {Object} props  properties
 */
const Table = props => (
  <table className="table table-striped">
    {props.children}
  </table>
);

export default Table;

Table.propTypes = {
  children: PropTypes.node,
};
