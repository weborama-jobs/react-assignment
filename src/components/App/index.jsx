import React from 'react';
import {Switch, Route} from 'react-router-dom';
import BeerList from 'components/BeerList';
/**
 * Begin, contains app routes
 */
const App = () =>
  (
    <Switch>
      <Route
        exact
        path="/"
        component={BeerList}
      />
    </Switch>
  );

export default App;
