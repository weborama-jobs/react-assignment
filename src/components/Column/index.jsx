import React from 'react';
import PropTypes from 'prop-types';
/**
 * Bootstrap Grid container
 * @param {Object} props props
 */
const Column = props => (
  <div className="col">
    {props.children}
  </div>
);

export default Column;

Column.propTypes = {
  children: PropTypes.node,
};
