import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import App from 'components/App';
/**
 * Entry point. Initializing Router and mounting to a DOM element
 */
render(
  (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  ), document.getElementById('main'),
);
